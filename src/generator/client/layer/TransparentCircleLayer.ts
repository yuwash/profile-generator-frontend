import { LayerDrawer } from "./Layer";

export type TransparentCircleLayer = {
  type: "TRANSPARENT_CIRCLE";
  center: number;
  width: number;
  radius: number;
};

export const drawTransparentCircle: LayerDrawer<TransparentCircleLayer> = (
  ctx,
  { center, radius, width }
) => {
  ctx.save();
  ctx.globalCompositeOperation = "destination-out"
  ctx.fillStyle = "#000";
  ctx.lineWidth = width;
  ctx.beginPath();
  ctx.arc(center, center, radius, 0, Math.PI * 2, true);
  ctx.stroke();
  ctx.restore();
};
