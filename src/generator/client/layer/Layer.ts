import { Ctx } from "../clientGenerator";

export type LayerDrawer<T> = (
  ctx: Ctx,
  layer: T,
  width: number,
  height: number
) => Promise<void> | void;
